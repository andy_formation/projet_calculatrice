let elementEcran; // = document.querySelector("#ecran");

let affichage = "";

let operation = null;

let precedent = 0;

window.onload = () => {
    elementEcran = document.querySelector("#ecran");

    let touches = document.querySelectorAll("button");

    for (let touche of touches) {
        touche.addEventListener("click", gererTouches);
    }
}

function gererTouches () {
    let touche = this.innerText;

    if (parseFloat(touche) >= 0) {
            affichage = affichage + touche;
            elementEcran.innerText = affichage;
    } else {
        switch(touche) {
            case "C":
                affichage = "0";
                elementEcran.innerText = "0";
                precedent = 0;
                
            case "+":
                precedent = (precedent === 0) ? parseFloat(affichage) : 
                calcul(precedent, parseFloat(affichage), operation);
                elementEcran.innerText = precedent;
                operation = touche;
                affichage = "";
                break;
            case "-":
                precedent = (precedent === 0) ? parseFloat(affichage) : 
                calcul(precedent, parseFloat(affichage), operation);
                elementEcran.innerText = precedent;
                operation = touche;
                affichage = "";
                break;
            case "*":
                precedent = (precedent === 0) ? parseFloat(affichage) : 
                calcul(precedent, parseFloat(affichage), operation);
                elementEcran.innerText = precedent;
                operation = touche;
                affichage = "";
                break;
            case "/":
                precedent = (precedent === 0) ? parseFloat(affichage) : 
                calcul(precedent, parseFloat(affichage), operation);
                elementEcran.innerText = precedent;
                operation = touche;
                affichage = "";
                break;
            case "=":
                precedent = (precedent === 0) ? parseFloat(affichage) : 
                calcul(precedent, parseFloat(affichage), operation);
                elementEcran.innerText = precedent;
                affichage = precedent;
                precedent = 0;
                break;

                
        }
    }
}

function calcul(nbr1, nbr2, operation) {
    nbr1 = parseInt(nbr1);
    nbr2 = parseInt(nbr2);
    if (operation ==="+") return nbr1 + nbr2;
    if (operation ==="-") return nbr1 - nbr2;
    if (operation ==="*") return nbr1 * nbr2;
    if (operation ==="/") return nbr1 / nbr2;
    
}